import swaggerJsdoc from 'swagger-jsdoc';
import swaggerUi from 'swagger-ui-express';
import { Router } from 'express';
import { config } from 'dotenv';

config();

const router = Router();

const options = {
  swaggerDefinition: {
    openapi: '3.0.0',
    basePath: '',
    info: {
      title: 'Play food delivery',
      version: '1.0.0',
      description:
          'Food delivery fight covid 2019',
      license: {
        name: 'Playtorium',
        url: 'https://playtorium.co.th/'
      },
      contact: {
        name: 'Vathunyoo',
        email: 'vathunyoo@playtorium.co.th'
      }
    },
    components: {
      securitySchemes: {
        bearerAuth: {
          type: 'http',
          scheme: 'bearer',
          bearerFormat: 'JWT'
        }
      }
    },
    security: {
      bearerAuth: []
    },
    servers: [
      {
        url: `http://localhost:${process.env.BACKEND_PORT}/api`,
        description: 'Local backend'
      },
      {
        url: 'https://docker-testing-env.playtorium.co.th/plays-food-delivery-backend/api',
        description: 'testing on develop'
      },
      {
        url: 'https://imyellow.playtorium.co.th/backend-pfd/api',
        description: 'imyellow backend (with load balancer)'
      },
      {
        url: 'https://imyellow-staging.playtorium.co.th/backend-pfd/api',
        description: 'staging imyellow backend (with load balancer)'
      },
      {
        url: 'https://sushihiro.playtorium.co.th/backend-pfd/api',
        description: 'sushihiro production'
      }
    ]
  },
  apis: [
    './server/swaggers/routes/test.yml',
    './server/swaggers/routes/status.yml',
    './server/swaggers/routes/customers/address.yml',
    './server/swaggers/routes/customers/branch.yml',
    './server/swaggers/routes/customers/coupon.yml',
    './server/swaggers/routes/customers/customer.yml',
    './server/swaggers/routes/customers/order.yml',
    './server/swaggers/routes/customers/chillPay.yml',
    './server/swaggers/routes/customers/product.yml',
    './server/swaggers/routes/customers/cart.yml',
    './server/swaggers/routes/drivers/address.yml',
    './server/swaggers/routes/drivers/driver.yml',
    './server/swaggers/routes/drivers/order.yml',
    './server/swaggers/routes/headOffices/address.yml',
    './server/swaggers/routes/headOffices/branch.yml',
    './server/swaggers/routes/headOffices/coupon.yml',
    './server/swaggers/routes/headOffices/profile.yml',
    './server/swaggers/routes/headOffices/category.yml',
    './server/swaggers/routes/headOffices/option.yml',
    './server/swaggers/routes/headOffices/choice.yml',
    './server/swaggers/routes/headOffices/product.yml',
    './server/swaggers/routes/headOffices/report.yml',
    './server/swaggers/routes/headOffices/setting.yml',
    './server/swaggers/routes/restaurants/address.yml',
    './server/swaggers/routes/restaurants/branch.yml',
    './server/swaggers/routes/restaurants/category.yml',
    './server/swaggers/routes/restaurants/choice.yml',
    './server/swaggers/routes/restaurants/coupon.yml',
    './server/swaggers/routes/restaurants/option.yml',
    './server/swaggers/routes/restaurants/order.yml',
    './server/swaggers/routes/restaurants/product.yml',
    './server/swaggers/routes/restaurants/report.yml',
    './server/swaggers/routes/restaurants/setting.yml',
    './server/swaggers/routes/restaurants/driver.yml',
    './server/swaggers/routes/restaurants/lalamove.yml',
    './server/swaggers/routes/customers/waiting.yml',
    './server/swaggers/routes/auth.yml',
    './server/swaggers/schemas/auth.yml',
    './server/swaggers/schemas/product.yml'
  ]
};

const specs = swaggerJsdoc(options);

router.use('/', swaggerUi.serve);
router.get('/', swaggerUi.setup(specs, {
  explorer: true
}));

export default router;