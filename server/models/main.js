import dateFormat from 'dateformat';
import change from 'change-case';
import multer from 'multer';
import { query } from 'express';
import changeCase from '../../utils/changeCase';
import {
  joinCondition,
  queryCondition,
  pagingCondition,
  insertFieldCondition,
  insertValueCondition,
  updateFieldCondition,
  updateValueCondition,
  conflictUpdateCondition,
  returningCondition
} from '../db/queryChecker';
import db from '../db';

const mainModel = {};

mainModel.find = async ({
  tableName = '',
  filters = [],
  join = {},
  conditions = {},
  group = [],
  having = {},
  sorts = [],
  direction = '',
  page = {},
  limit = 0,
  offset = 0
},
  pool = null
) => {
  try {
    if (!tableName) {
      throw new Error('table name is undefined');
    }

    if (!filters || !Array.isArray(filters)) {
      throw new Error('filters parameter requires array');
    }

    if (!join || typeof join !== 'object' || Array.isArray(join)) {
      throw new Error('join parameter requires object');
    }

    if (!conditions || typeof conditions !== 'object' || Array.isArray(conditions)) {
      throw new Error('conditions parameter requires object');
    }

    if (!group || !Array.isArray(group)) {
      throw new Error('group parameter requires array');
    }

    if (!having || typeof having !== 'object' || Array.isArray(having)) {
      throw new Error('having parameter requires object');
    }

    if (!sorts || !Array.isArray(sorts)) {
      throw new Error('sorts parameter requires array');
    }

    if (typeof direction !== 'string') {
      throw new Error('direction parameter requires string');
    }

    if (!page || typeof page !== 'object' || Array.isArray(page)) {
      throw new Error('page parameter requires object');
    }
    if (typeof limit !== 'number') {
      throw new Error('limit parameter requires number');
    }

    if (typeof offset !== 'number') {
      throw new Error('offset parameter requires number');
    }

    const queryString = `SELECT ${filters.length > 0 ? filters.join(', ') : '*'}`
      + ` FROM ${tableName}`
      + `${Object.entries(join).length > 0 ? ` ${joinCondition(join)}` : ''}`
      + `${Object.entries(conditions).length > 0 ? ` WHERE ${queryCondition(conditions)}` : ''}`
      + `${group.length > 0 ? ` GROUP BY ${group.join(', ')}` : ''}`
      + `${Object.entries(having).length > 0 ? ` HAVING ${queryCondition(having)}` : ''}`
      + `${sorts.length > 0 ? ` ORDER BY ${sorts.join(', ')}` : ''}`
      + `${['ASC', 'DESC'].includes(direction) ? ` ${direction}` : ''}`
      + `${limit ? ` LIMIT ${limit}` : ''}`
      + `${offset ? ` OFFSET ${offset}` : ''}`
      + `${Object.entries(page).length > 0 ? ` ${pagingCondition(page)}` : ''}`;

    console.log('****************find*********************');
    console.log('query string : ', queryString);

    let result = '';
    if (pool === null) {
      result = await db.query(queryString);
    } else {
      result = await pool.query(queryString);
    }
    result = changeCase.objInArrCamel(result);
    return result;
  } catch (err) {
    throw err;
  }
};

mainModel.query = async (query = null, pool = null) => {
  try {
    if (!query) {
      throw new Error('query is undefined');
    }

    let result = '';
    if (pool === null) {
      result = await db.query(query);
    } else {
      result = await pool.query(query);
    }
    result = changeCase.objInArrCamel(result);
    return result;
  } catch (err) {
    throw err;
  }
};

mainModel.insertConflictUpdate = async ({
  tableName = '',
  fields = [],
  conflictKeys = [],
  returning = []
},
  pool = null) => {
  try {
    if (!tableName) {
      throw new Error('table name is undefined');
    }
    /* Undified fields filter */
    Object.keys(fields[0]).forEach((key) => {
      if (fields[0][key] === undefined) {
        delete fields[0][key];
      }
    });

    if (fields.length === 0) {
      throw new Error('Field is undefined');
    }

    if (!fields || !Array.isArray(fields)) {
      throw new Error('fields parameter requires array');
    }

    if (!conflictKeys || !Array.isArray(conflictKeys)) {
      throw new Error('conflictKeys parameter requires array');
    }

    if (!returning || !Array.isArray(returning)) {
      throw new Error('returning parameter requires array');
    }

    let columns = Object.keys(fields[0]);
    columns = columns.map(column => change.snakeCase(column));
    conflictKeys = conflictKeys.map(column => change.snakeCase(column));

    const queryString = `INSERT INTO "${tableName}"`
    + ` ${insertFieldCondition(columns)}`
    + ` VALUES ${insertValueCondition(columns, fields)}`
    + `${conflictKeys.length > 0 ? ` ON CONFLICT (${conflictKeys.join(', ')}) DO UPDATE SET ${conflictUpdateCondition(conflictKeys, columns)}` : ''}`
    + ` RETURNING ${returning.length > 0 ? `${returningCondition(returning)}` : '*'}`;

    let result = '';
    if (pool === null) {
      result = await db.query(queryString);
    } else {
      result = await pool.query(queryString);
    }
    result = changeCase.objInArrCamel(result);

    return result;
  } catch (err) {
    throw err;
  }
};

mainModel.update = async ({
  tableName = '',
  fields = {},
  conditions = {},
  returning = []
},
  pool = null) => {
  try {
    if (!tableName) {
      throw new Error('table name is undefined');
    }

    if (!returning || !Array.isArray(returning)) {
      throw new Error('returning parameter requires array');
    }

    let columns = Object.keys(fields);
    columns = columns.map(column => change.snakeCase(column));

    const queryString = `UPDATE "${tableName}"`
    + ` SET ${updateFieldCondition(columns)} = ${updateValueCondition(fields)}`
    + ` WHERE ${queryCondition(conditions)}`
    + ` RETURNING ${returning.length > 0 ? `${returningCondition(returning)}` : '*'}`;

    console.log('*****************update******************');
    console.log(queryString);

    let result = '';
    if (pool === null) {
      result = await db.query(queryString);
    } else {
      result = await pool.query(queryString);
    }
    result = changeCase.objInArrCamel(result);
    return result;
  } catch (err) {
    throw err;
  }
};

mainModel.delete = async ({
  tableName = '',
  conditions = {},
  returning = []
},
  pool = null) => {
  try {
    if (!tableName) {
      throw new Error('table name is undefined');
    }

    if (!conditions || typeof conditions !== 'object' || Array.isArray(conditions)) {
      throw new Error('conditions parameter requires object');
    }

    if (!returning || !Array.isArray(returning)) {
      throw new Error('returning parameter requires array');
    }

    const queryString = `DELETE FROM "${tableName}"`
    + ` WHERE ${queryCondition(conditions)}`
    + ` RETURNING ${returning.length > 0 ? `${returningCondition(returning)}` : '*'}`;

    let result = '';
    if (pool === null) {
      result = await db.query(queryString);
    } else {
      result = await pool.query(queryString);
    }
    result = changeCase.objInArrCamel(result);
    return result;
  } catch (err) {
    throw err;
  }
};

export default mainModel;