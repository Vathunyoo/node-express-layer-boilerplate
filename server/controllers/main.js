import mainService from '../services/main';

const mainController = {};

mainController.intial = async (req, res, next) => {
  const text = await mainService.initial();
  res.status(200).send(text);
};

export default mainController;