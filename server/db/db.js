import { Pool } from 'pg';
import { config } from 'dotenv';

config();

const dbConfig = {
  host: process.env.DB_HOST,
  database: process.env.DB_NAME,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT
};

if (process.env.DB_SSL) {
  dbConfig.ssl = (process.env.DB_SSL.toLowerCase() === 'true');
}

export default new Pool(dbConfig);