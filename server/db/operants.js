const operants = {
  between: 'BETWEEN',
  equal: '=',
  notequal: '!=',
  lessthan: '<',
  atmost: '<=',
  morethan: '>',
  atleast: '>=',
  like: 'ILIKE',
  in: 'IN',
  notin: 'NOT IN',
  notlike: 'NOT ILIKE',
  is: 'IS',
  isnot: 'IS NOT',
  range: 'n/a',
  numericrange: 'n/a',
  rlike: '~*',
  notrlike: '!~*'
};

export default operants;