import change from 'change-case';
import operants from './operants';
import joins from './joins';

// Ambiguous case include '.', ' ', '::'

const ambiguousClarifier = ({
  list = [],
  tableName = '',
  type = null,
  join = {}
}) => {
  if (type) {
    let tableAlias = null;
    tableName = tableName.replace(new RegExp('\\s+', 'g'), ' ');

    if (tableName.split(' ')[1] && !tableAlias) {
      tableAlias = tableName.split(' ')[1];
    }

    if (type === 'filter') {
      let tableJoinNames = [];
      // In case have join
      if (Object.entries(join).length > 0) {
        tableJoinNames = Object.keys(join).map(tableJoinName => tableJoinName.split(' ')[1]);
      }
      for (let i = 0; i < list.length; i += 1) {
        if (typeof list[i] === 'string') {
          const field = list[i].split('.');
          // In case query have join (check all field in table with join in case alias)
          if (field[1]) {
            if (tableAlias && Object.entries(join).length === 0 && tableAlias !== field[0]) {
              throw new Error(`field (${list[i]}) don't match table alias`);
            } else if (tableAlias && Object.entries(join).length > 0 && tableAlias !== field[0] && !tableJoinNames.includes(field[0])) {
              throw new Error(`field (${list[i]}) don't match table join alias`);
            }
          }

          const aliasField = list[i].replace(new RegExp('\\s+', 'g'), ' ').split(' ');
          if (aliasField[1]) {
            list[i] = `${aliasField[0]} AS ${aliasField[1]}`;
          }
        } else {
          throw new Error('Element in list should be string');
        }
      }

      return list;
    }
  } else {
    throw new Error('type is undefined');
  }
};

export const filterCondition = (
  filters = []
) => {
  try {
    return filters.join(', ');
  } catch (err) {
    throw err;
  }
};

export const joinCondition = (join) => {
  const joinList = [];
  const tableJoinPair = Object.entries(join);
  for (let i = 0; i < tableJoinPair.length; i += 1) {
    const [table, pair] = tableJoinPair[i];

    const joinType = Object.keys(pair)[0];
    const values = pair[Object.keys(pair)[0]];

    if (values.length !== 2) {
      throw new Error("Can't pair key in join (pair value should be 2)");
    }
    joinList.push(`${joins[joinType]} ${table} ON ${values[0]} = ${values[1]}`);
  }
  return joinList.join(' ');
};

export const queryCondition = (condition) => {
  try {
    const conditionList = [];
    const conditionPairs = Object.entries(condition);
    for (let i = 0; i < conditionPairs.length; i += 1) {
      const [field, operation] = conditionPairs[i];
      const [operant, value] = Object.entries(operation)[0];

      let newValue = '';
      if (typeof value === 'string' && !/^\d{4}-\d{2}-\d{2} (\d{2}):(\d{2}):(\d{2})$/.test(value)) {
        if (value.includes('(') && operant === 'in') {
          // in case IN ()
          newValue = value;
        } else if (value.includes("'")) {
          // normal string
          // check case nestesd query -> where a = (select * from b)
          if (value.includes('SELECT') && value.includes('FROM') && value.includes('(')) {
            newValue = `${value}`;
          } else {
            newValue = `'${value.replace(/'/g, "''")}'`;
          }
        } else {
          newValue = `'${value}'`;
        }
      } else if (/^\d{4}-\d{2}-\d{2} (\d{2}):(\d{2}):(\d{2})$/.test(value)) {
        // timestamp
        newValue = `'${value}'::timestamp`;
      } else if (typeof value === 'number' || typeof value === 'boolean') {
        newValue = value;
      } else if (typeof value === 'object') {
        if (Array.isArray(value)) {
          if (value.length === 2) {
            if (/^\d{4}-\d{2}-\d{2} (\d{2}):(\d{2}):(\d{2})$/.test(value[0]) && /^\d{4}-\d{2}-\d{2} (\d{2}):(\d{2}):(\d{2})$/.test(value[1])) {
              newValue = `'${value[0]}'::timestamp AND '${value[1]}'::timestamp`;
            } else {
              newValue = `${value[0]} AND ${value[1]}`;
            }
          }
        } else {
          // object not array (null)
          newValue = value;
        }
      } else {
        throw new Error('type of value is undefined');
      }
      conditionList.push(`${field} ${operants[operant]} ${newValue}`);
    }
    return conditionList.join(' AND ');
  } catch (err) {
    throw err;
  }
};

export const pagingCondition = (page) => {
  try {
    if (page.page.toString() && page.pageSize.toString()) {
      return `LIMIT ${page.pageSize} OFFSET ${page.page * page.pageSize}`;
    }
    throw new Error('page or page size are undefined');
  } catch (err) {
    throw err;
  }
};

export const insertFieldCondition = (columns) => {
  try {
    columns = columns.map(column => `"${column}"`);
    return `(${columns.join(', ')})`;
  } catch (err) {
    throw err;
  }
};

export const insertValueCondition = (columns, fields) => {
  try {
    const valueArr = [];
    for (const [i, field] of fields.entries()) {
      let columnFields = Object.keys(field);
      columnFields = columnFields.map((column, j) => {
        column = change.snakeCase(column);
        if (column !== columns[j]) {
          throw new Error(`key column (${columns[i]}) don't match value column (${column}) at field[${i}][${j}]`);
        }
        return column;
      });

      if (columns.length !== columnFields.length) {
        throw new Error(`insert value field length (${columnFields.length}) not equal column field length (${columns.length}) at fields : ${i}`);
      }

      const valueFields = Object.values(field).map((value) => {
        if (typeof value === 'string') {
          const regex = RegExp(/^\d{4}-\d{2}-\d{2} (\d{2}):(\d{2}):(\d{2})$/);
          if (regex.test(value)) {
            value = `'${value}'::timestamp`;
          } else if (value.includes("'")) {
            value = `'${value.replace(/'/g, "''")}'`;
          } else {
            value = `'${value}'`;
          }
        }

        if (typeof value === 'object') {
          value = 'null';
        }

        return value;
      });

      valueArr.push(`(${valueFields.join(', ')})`);
    }

    return valueArr.join(', ');
  } catch (err) {
    throw err;
  }
};

export const updateFieldCondition = (columns) => {
  try {
    columns = columns.map(column => `"${column}"`);
    if (columns.length === 1) {
      return columns;
    }
    return `(${columns.join(', ')})`;
  } catch (err) {
    throw err;
  }
};

export const updateValueCondition = (fields) => {
  try {
    const valueArr = [];
    const valueFields = Object.values(fields).map((value) => {
      if (typeof value === 'string') {
        const regex = RegExp(/^\d{4}-\d{2}-\d{2} (\d{2}):(\d{2}):(\d{2})$/);
        if (regex.test(value)) {
          value = `'${value}'::timestamp`;
        } else if (value.includes("'")) {
          value = `'${value.replace(/'/g, "''")}'`;
        } else {
          value = `'${value}'`;
        }
      }

      if (typeof value === 'object') {
        value = 'null';
      }

      return value;
    });

    if (valueFields.length === 1) {
      valueArr.push(valueFields);
    } else {
      valueArr.push(`(${valueFields.join(', ')})`);
    }

    return valueArr;
  } catch (err) {
    throw err;
  }
};

export const conflictUpdateCondition = (conflicts, columns) => {
  try {
    return columns.filter(column => !conflicts.includes(column)).map(column => `"${column}" = excluded."${column}"`).join(', ');
  } catch (err) {
    throw err;
  }
};

export const returningCondition = (columns) => {
  try {
    return columns.map(column => `"${change.snakeCase(column)}"`).join(', ');
  } catch (err) {
    throw err;
  }
};

export const test = '';