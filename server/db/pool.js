import pool from './db';

class Pool {
  poolChecker() {
    if (!this.client) {
      const err = new Error(
        'pool not found, use "pool.begin()" to init pool first'
      );
      err.status = 500;

      throw err;
    }
  }

  async begin() {
    try {
      this.client = await pool.connect();

      await this.client.query('BEGIN');
    } catch (err) {
      throw err;
    }
  }

  async commit() {
    this.poolChecker();
    try {
      await this.client.query('COMMIT');

      await this.client.release();
    } catch (err) {
      throw err;
    }
  }

  async rollback() {
    this.poolChecker();
    try {
      await this.client.query('ROLLBACK');
    } catch (err) {
      throw err;
    } finally {
      await this.client.release();
    }
  }

  async query(queryString) {
    try {
      this.poolChecker();
      const { rows } = await this.client.query(queryString);
      if (!rows) {
        const err = new Error('response error: no any output rows.');
        throw err;
      }

      return rows;
    } catch (err) {
      throw err;
    }
  }

  async one(queryString) {
    try {
      const { rows, rowCount } = await this.client.query(queryString);

      if (!rows) {
        const err = new Error('response error: no any output row.');
        throw err;
      }

      if (rowCount !== 1) {
        const err = new Error('response error: response row number is not 1.');
        throw err;
      }

      return rows;
    } catch (err) {
      throw err;
    }
  }

  async oneOrNone(queryString) {
    try {
      const { rows, rowCount } = await this.client.query(queryString);

      if (!rows) {
        const err = new Error('response error: no any output rows.');
        throw err;
      }

      if (rowCount > 1) {
        const err = new Error(
          'response error: response row number more than 1.'
        );
        throw err;
      }

      return rows;
    } catch (err) {
      throw err;
    }
  }
}

export default Pool;