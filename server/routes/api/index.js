import { Router } from 'express';
import swaggerDoc from '../../swaggers';
import auth from './auth';
import mainController from '../../controllers/main';

const router = Router();

router.use('/docs', swaggerDoc);
// router.use('/auth', auth);

router.get('/main', mainController.intial);

export default router;
