import { Router } from 'express';
import passport from 'passport';
import cors from 'cors';
// import auth from './auth';

const router = Router();

// router.user('/auth', auth);
router.get('/login/google', passport.authenticate('google', {
  scope: ['email', 'profile', 'openid']
}));

router.get('/login/facebook', passport.authenticate('facebook', { scope: ['email'] }));

// router.get('/google/callback', passport.authenticate('google', { failureRedirect: '/login' }), authController.generateTokenFromGoogle);

// router.get('/facebook/callback', passport.authenticate('facebook', { failureRedirect: '/login' }), authController.generateTokenFromFacebook);

router.options('/login/email', cors()); // enable pre-flight request
export default router;