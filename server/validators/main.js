import { check } from 'express-validator';

const addressValidator = {};


addressValidator.insert = () => {
  return [
    check('name')
      .isString()
      .withMessage('name should be string')
      .isLength({ max: 100 })
      .withMessage('name should not longer than 100 characters'),
    check('addressLine')
      .isString()
      .withMessage('addressLine should be string')
      .isLength({ max: 100 })
      .withMessage('addressLine should not longer than 100 characters'),
    check('province')
      .isString()
      .withMessage('province should be string')
      .isLength({ max: 100 })
      .withMessage('province should not longer than 100 characters'),
    check('district')
      .isString()
      .withMessage('district should be string')
      .isLength({ max: 100 })
      .withMessage('district should not longer than 100 characters'),
    check('subDistrict')
      .isString()
      .withMessage('subDistrict should be string')
      .isLength({ max: 100 })
      .withMessage('subDistrict should not longer than 100 characters'),
    check('postalCode')
      .isLength({ min: 5, max: 5 })
      .withMessage('postalCode should contain only 5 number')
      .isNumeric()
      .withMessage('postalCode should contain only 5 number'),
    check('latitude')
      .isNumeric()
      .withMessage('latitude and longtitude should be in the number format'),
    check('longitude')
      .isNumeric()
      .withMessage('longitude and longtitude should be in the number format'),
    check('comment')
      .optional(),
    check('isDefault')
      .isBoolean()
      .withMessage('isDefault should be boolean')
  ];
};

addressValidator.delete = () => {
  return [
    check('addressId')
      .isInt({ min: 1 })
      .withMessage('addressId should be int and more than 0.')
      .isLength({ max: 10 })
      .withMessage('addressId should not longer than 10 digits.')
  ];
};

addressValidator.edit = () => {
  return [
    check('addressId')
      .isInt({ min: 1 })
      .withMessage('addressId should be int and more than 0.')
      .isLength({ max: 10 })
      .withMessage('addressId should not longer than 10 digits.'),
    check('name')
      .optional()
      .isString()
      .withMessage('name should be string')
      .isLength({ max: 100 })
      .withMessage('name should not longer than 100 characters'),
    check('addressLine')
      .optional()
      .isString()
      .withMessage('addressLine should be string')
      .isLength({ max: 100 })
      .withMessage('addressLine should not longer than 100 characters'),
    check('province')
      .optional()
      .isString()
      .withMessage('province should be string')
      .isLength({ max: 100 })
      .withMessage('province should not longer than 100 characters'),
    check('district')
      .optional()
      .isString()
      .withMessage('district should be string')
      .isLength({ max: 100 })
      .withMessage('district should not longer than 100 characters'),
    check('subDistrict')
      .optional()
      .isString()
      .withMessage('subDistrict should be string')
      .isLength({ max: 100 })
      .withMessage('subDistrict should not longer than 100 characters'),
    check('postalCode')
      .optional()
      .isLength({ min: 5, max: 5 })
      .withMessage('postalCode should contain only 5 number')
      .isNumeric()
      .withMessage('postalCode should contain only 5 number'),
    check('latitude')
      .optional()
      .isNumeric()
      .withMessage('latitude and longtitude should be in the number format'),
    check('longitude')
      .optional()
      .isNumeric()
      .withMessage('longitude and longtitude should be in the number format'),
    check('comment')
      .optional(),
    check('isDefault')
      .optional()
      .isBoolean()
      .withMessage('isDefault should be boolean')
  ];
};

addressValidator.setDefault = () => {
  return [
    check('addressId')
      .isInt({ min: 1 })
      .withMessage('addressId should be int and more than 0.')
      .isLength({ max: 10 })
      .withMessage('addressId should not longer than 10 digits.')
  ];
};

addressValidator.addDistanceToBranch = () => {
  return [
    check('addressId')
      .isInt({ min: 1 })
      .withMessage('addressId should be int and more than 0.')
      .isLength({ max: 10 })
      .withMessage('addressId should not longer than 10 digits.')
  ];
};

export default addressValidator;