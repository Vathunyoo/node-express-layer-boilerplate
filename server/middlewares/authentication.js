import boom from '@hapi/boom';
import passport from './passport';

const authen = {};

authen.auth = passport.authenticate('jwt', { session: false });

authen.role = (req, res, next) => {
  try {
    const role = req.user.role;
    const rolePath = req.originalUrl.split('/')[2];

    if (!req.originalUrl.split('/')[2]) {
    //   throw new Error("Can't find role in url path");
      throw boom.unauthorized("Can't find role in url path");
    }

    if (role === 'C') {
      if (rolePath !== 'customer') {
        throw boom.unauthorized(`Role don't match (role : ${rolePath})`);
      }
    } else if (role === 'H') {
      if (rolePath !== 'headOffice') {
        throw boom.unauthorized(`Role don't match (role : ${rolePath})`);
      }
    } else if (role === 'R') {
      if (rolePath !== 'restaurant') {
        throw boom.unauthorized(`Role don't match (role : ${rolePath})`);
      }
    } else if (role === 'D') {
      if (rolePath !== 'driver') {
        throw boom.unauthorized(`Role don't match (role : ${rolePath})`);
      }
    } else {
    //   throw new Error("Don't match any role in system");
      throw boom.unauthorized("Don't match any role in system");
    }

    next();
  } catch (err) {
    next(err);
  }
};

export default authen;