import { logErrStack } from '../../utils/logger/logger';
import errMessage from '../../utils/errorHandle/errMessage';
import errStatusCode from '../../utils/errorHandle/errStatusCode';

const errorHandle = (err, req, res, next) => {
  res.status(err.customCode ? errStatusCode[err.customCode] : 500).json({
    message: err.customCode ? errMessage[err.customCode] : errMessage[3000],
    customCode: err.customCode ? err.customCode : 3000,
    data: err.data || undefined
  });
};

export default errorHandle;
