import config from '../config';
import { logInfo } from '../utils/logger/logger';
import app from './app';

const PORT = config.server.port;
app.listen(PORT, async () => {
  logInfo(`Server start at port : ${config.server.port}`);
});