import { config } from 'dotenv';

config();

const setting = {
  server: {
    port: process.env.PORT || 3000
  },
  logger: {
    level: process.env.LOGGER_LEVEL || 'info'
  },
  db: {
    postgres: {
      main: {
        host: process.env.DB_HOST || '',
        database: process.env.DB_NAME || '',
        user: process.env.DB_USER || '',
        password: process.env.DB_PASSWORD || '',
        port: process.env.DB_PORT || ''
      }
    }
  }
};

export default setting;