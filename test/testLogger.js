import { logInfo, logErr } from '../utils/logger/logger';
import LogStep from '../utils/logger/logStep';

const loggie = new LogStep('appium', 10);
console.log(loggie);

loggie.mainStep('should be start first');
loggie.subStep('hello world start');

logInfo('hello world');
logErr('hello');