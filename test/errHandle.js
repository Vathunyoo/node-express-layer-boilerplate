import boom from '@hapi/boom';
import { spawn } from 'child_process';
import CustomError from '../utils/customError';
import errMessage from '../utils/errMessage';

const main = async () => {
  try {
    // const testPromise = new Promise((resolve, reject) => {
    //   const adbCommand = spawn('adbd', ['devices'], {
    //     detached: true
    //   });

    //   adbCommand.stdout.on('data', (data) => {
    //     const datas = data.toString();
    //     resolve(datas);
    //   });

    //   adbCommand.stderr.on('data', (data) => {
    //     reject(new CustomError({
    //       message: 'adb command list failed',
    //       data: data.toString()
    //     }));
    //   });

    //   adbCommand.on('error', (err) => {
    //     console.log('err : ', err);
    //     reject(new CustomError({
    //       message: 'adb command list failed',
    //       data: err.toString()
    //     }));
    //   });
    // });

    // const result = await testPromise;

    // run appium
    const testPromise = new Promise((resolve, reject) => {
      const appium = spawn('appium', ['-p', '4725'], {
        detached: true
      });
      appium.stdout.on('data', (data) => {
        console.log('appium pid : ', appium.pid);
        resolve(data.toString());
      });
    });

    const result = await testPromise;
    console.log('result : ', result);
  } catch (err) {
    console.log('err : ', err);
  }
};

main();
