module.exports = {
  "extends": "airbnb-base",
"rules": {
"no-console": 0,
"no-restricted-syntax": 0,
"no-param-reassign": [2, {"props": false}],
"prefer-destructuring": 0,
"treatUndefinedAsUnspecified": true,
"arrow-body-style": 0,
"comma-dangle": 0,
"eol-last": 0,
"import/no-dynamic-require": 0,
"consistent-return": 0,
"guard-for-in": 0,
"no-await-in-loop": 0,
"no-loop-func": 0
},
"env": {
"commonjs": true,
"node": true,
"mocha": true
},
};