import {
  spawn, exec, execSync
} from 'child_process';
import path from 'path';
import moment from 'moment-timezone';
import CustomError from '../errorHandle/customError';
import cmdTool from '../childProcess/cmdTool';
import SpawnHandle from '../childProcess/childProcess';
import { mappedDeviceProp, mappedDeviceName } from './mappedDeviceProp';

const adbService = {};

adbService.list = () => {
  return new Promise((resolve, reject) => {
    const adbCommand = spawn('adb', ['devices'], {
      detached: true
    });

    const adbProcess = new SpawnHandle(adbCommand, 'adb devices', reject);

    adbProcess.onStdoutData();
    adbProcess.onStderrData();
    adbProcess.onExit((code, data) => {
      if (!code) {
        let results = data;
        results.shift();
        results = results.filter(result => result);
        results = results.map((result) => {
          return result.split('\t')[0];
        });
        resolve(results);
      }
    });
  });
};

adbService.getProp = (udid, filters) => {
  return new Promise(async (resolve, reject) => {
    const command = [
      '-s',
      udid,
      'shell',
      'getprop'
    ];

    const result = {};

    try {
      if (filters && filters.length) {
        await Promise.all(filters.map(async (filter) => {
          try {
            if (filter === 'resolution') {
              result.resolution = await adbService.getResolution(udid);
            } else if (filter === 'deviceName') {
              result.deviceName = await adbService.getDeviceName(udid);
            } else if (mappedDeviceProp[filter]) {
              if (command.length === 3) {
                command.push('|', 'grep');
              }
              command.push('-e', mappedDeviceProp[filter]);
            } else {
              throw new Error("Can't find device prop");
            }
          } catch (err) {
            throw err;
          }
        }));
      } else {
        // send all props in map if filter is undefined
        command.push('|', 'grep');
        Object.keys(mappedDeviceProp).forEach((prop) => {
          command.push('-e', mappedDeviceProp[prop]);
        });
        result.resolution = await adbService.getResolution(udid);
        result.deviceName = await adbService.getDeviceName(udid);
      }
    } catch (err) {
      reject(err);
    }

    const adbCommand = spawn('adb', command, {
      detached: true
    });

    const adbProcess = new SpawnHandle(adbCommand, 'adb getprop', reject);

    adbProcess.onStdoutData();
    adbProcess.onStderrData();
    adbProcess.onExit((code, data) => {
      if (!code) {
        data.forEach((deviceProp) => {
          const keyPair = deviceProp.replace(/\[|\]/g, '').split(': ');
          result[mappedDeviceName[keyPair[0]]] = keyPair[1];
        });
        resolve(result);
      }
    });
  });
};

adbService.getResolution = (udid) => {
  return new Promise((resolve, reject) => {
    const adbCommand = spawn('adb', ['-s', udid, 'shell', 'wm', 'size'], {
      detached: true
    });

    const adbProcess = new SpawnHandle(adbCommand, 'adb get resolution', reject);

    adbProcess.onStdoutData();
    adbProcess.onStderrData();
    adbProcess.onExit((code, data) => {
      if (!code) {
        resolve(data[0].split(': ')[1]);
      }
    });
  });
};

adbService.getDeviceName = (udid) => {
  return new Promise((resolve, reject) => {
    const args = ['-c', `adb -s ${udid} shell dumpsys bluetooth_manager | grep "name:"`];
    const adbCommand = spawn('sh', args, {
      detached: true
    });

    const adbProcess = new SpawnHandle(adbCommand, 'adb get device name', reject);

    adbProcess.onStdoutData();
    adbProcess.onStderrData();
    adbProcess.onExit((code, data) => {
      if (!code) {
        resolve(data[0].split(': ')[1]);
      }
    });
  });
};

adbService.captureScreenAndPull = (udid, destination) => {
  return new Promise((resolve, reject) => {
    const args = ['-c', `adb -s ${udid} shell screencap -p > ${destination}`];
    const adbCommand = spawn('sh', args, {
      detached: true
    });

    const adbProcess = new SpawnHandle(adbCommand, 'adb screencapture and pull', reject);

    adbProcess.onStdoutData();
    adbProcess.onStderrData();
    adbProcess.onError();
    adbProcess.onExit((code, data, err, pid) => {
      if (!code) {
        resolve({
          pid
        });
      }
    });
  });
};

adbService.startScreenRecord = async (udid) => {
  try {
    const videoDevicePath = `sdcard/video_${moment.utc(new Date()).tz('Asia/Bangkok').format('YYYY_MM_DD_HH:mm:ss')}.mp4`;
    const adbProcess = exec(`adb -s ${udid} shell screenrecord ${videoDevicePath}`);
    return {
      udid,
      pid: adbProcess.pid
    };
  } catch (err) {
    throw err;
  }
};

adbService.stopScreenRecord = async (udid) => {
  try {
    const pidScreenRecordProcess = await cmdTool.findPidByCommand(`adb -s ${udid} shell screenrecord`);
    if (pidScreenRecordProcess.pid) {
      return await cmdTool.killProcessByPid(pidScreenRecordProcess.pid);
    }
    throw new Error('Not found err process');
  } catch (err) {
    throw err;
  }
};

adbService.pullFile = async ({
  udid,
  devicePath,
  destination
}) => {
  const adbProcess = exec(`adb -s ${udid} pull ${devicePath} ${destination}`);
  return adbProcess.pid;
};

export default adbService;