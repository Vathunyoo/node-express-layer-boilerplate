const mappedDeviceProp = {
  platformVersion: 'ro.build.version.release',
  udid: 'ro.serialno',
  platformName: 'net.bt.name',
  modelName: 'ro.product.model',
  manufacturer: 'ro.product.manufacturer'
};

const mappedDeviceName = {
  'ro.build.version.release': 'platformVersion',
  'ro.serialno': 'udid',
  'net.bt.name': 'platformName',
  'ro.product.model': 'modelName',
  'ro.product.manufacturer': 'manufacturer'
};

export { mappedDeviceProp, mappedDeviceName };