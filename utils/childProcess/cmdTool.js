import { spawn } from 'child_process';
import CustomError from '../errorHandle/customError';
import SpawnHandle from './childProcess';

const cmdTool = {};

cmdTool.findPidByPort = (port) => {
  return new Promise((resolve, reject) => {
    const args = ['-c', `lsof -i :${port} | awk 'NR!=1 {print $2}'`];
    const command = spawn('sh', args, {
      detached: true
    });

    const commandProcess = new SpawnHandle(command, 'find pid by port', reject);

    commandProcess.onStdoutData();
    commandProcess.onStderrData();
    commandProcess.onError();
    commandProcess.onExit((code, data) => {
      if (!code && data.length) {
        resolve({
          pid: data[0]
        });
      } else if (!code && !data.length) {
        resolve({
          pid: ''
        });
      }
    });
  });
};

cmdTool.findPidByCommand = (commandText) => {
  return new Promise((resolve, reject) => {
    const args = ['-c', `ps | grep "${commandText}" | awk 'NR==1 {print $1}'`];
    const command = spawn('sh', args, {
      detached: true
    });

    const commandProcess = new SpawnHandle(command, 'find pid by command', reject);

    commandProcess.onStdoutData();
    commandProcess.onStderrData();
    commandProcess.onError();
    commandProcess.onExit((code, data) => {
      if (!code && data.length) {
        resolve({
          pid: data[0]
        });
      } else if (!code && !data.length) {
        resolve({
          pid: ''
        });
      }
    });
  });
};

cmdTool.killProcessByPid = (pid, signalValue) => {
  return new Promise((resolve, reject) => {
    const command = spawn('kill', [`${signalValue || '-9'}`, pid], {
      detached: true
    });

    const commandProcess = new SpawnHandle(command, 'kill process by pid', reject);
    commandProcess.onStdoutData();
    commandProcess.onStderrData();
    commandProcess.onError();
    commandProcess.onExit((code) => {
      if (!code) {
        resolve({
          pid
        });
      }
    });
  });
};

export default cmdTool;