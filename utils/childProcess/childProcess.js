import CustomError from '../errorHandle/customError';

class SpawnHandle {
  constructor(childProcess, command, reject, resolve) {
    this.childProcess = childProcess;
    this.pid = childProcess.pid;
    this.datas = [];
    this.errors = [];
    this.command = command;
    this.resolve = resolve;
    this.reject = reject;
  }

  // When command execute finish
  onStdoutData(modifyDataFunc) {
    this.childProcess.stdout.on('data', (data) => {
      if (modifyDataFunc) {
        modifyDataFunc(data.toString(), this.pid);
      } else {
        this.datas.push(data.toString());
      }
    });
  }

  // When command execute finish but got some error
  onStderrData(modifyDataFunc) {
    this.childProcess.stderr.on('data', (err) => {
      if (modifyDataFunc) {
        modifyDataFunc(err.toString(), this.pid);
      } else {
        this.errors.push(err.toString());
      }
    });
  }

  onError(modifyDataFunc) {
    this.childProcess.on('error', (err) => {
      if (modifyDataFunc) {
        modifyDataFunc(err);
      } else {
        this.reject(new CustomError({
          message: `command (${this.command}) error with message : ${err}`,
          data: this.errors,
          customCode: 3000
        }));
      }
    });
  }

  // When command exit
  onExit(modifyDataFunc) {
    this.childProcess.on('exit', (code) => {
      if (this.datas.length) {
        this.datas = this.datas.reduce((accumulator, currentData) => {
          let newData = currentData.split('\n');
          newData = newData.filter(current => current);

          return accumulator.concat(newData);
        }, []);
      }

      if (this.errors.length) {
        this.errors = this.errors.reduce((accumulator, currentData) => {
          let newData = currentData.split('\n');
          newData = newData.filter(current => current);

          return accumulator.concat(newData);
        }, []);
      }

      if (modifyDataFunc && this.reject) {
        modifyDataFunc(code, this.datas, this.errors, this.pid);
        this.reject(new CustomError({
          message: `command (${this.command}) is failed`,
          customCode: 3000,
          data: this.errors,
          statusCode: 500
        }));
      } else if (modifyDataFunc && !this.reject) {
        modifyDataFunc(code, this.datas, this.errors, this.pid);
      } else if (!modifyDataFunc && !code) {
        this.resolve(this.datas);
      } else if (code) {
        this.reject(new CustomError({
          message: `command (${this.command}) exits with code : ${code}`,
          data: this.errors,
          customCode: 3000,
          statusCode: 500
        }));
      }
    });
  }
}

export default SpawnHandle;