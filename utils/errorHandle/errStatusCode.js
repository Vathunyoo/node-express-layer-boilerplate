const errStatusCode = {
  1001: 400,
  1002: 400,
  1003: 400,
  1004: 500,
  3000: 500
};

export default errStatusCode;