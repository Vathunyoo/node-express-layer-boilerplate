const errMessage = {
  1001: 'Invalid parameter',
  1002: 'Data is already exists',
  1003: 'Data cannot be found',
  1004: 'Connection refused by server',
  3000: 'System Error'
};

export default errMessage;