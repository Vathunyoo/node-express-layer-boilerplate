class CustomError extends Error {
  constructor({
    message, customCode, data, statusCode
  }) {
    super(message || '');
    this.customCode = customCode;
    this.data = data;
    this.statusCode = statusCode;
  }
}

export default CustomError;