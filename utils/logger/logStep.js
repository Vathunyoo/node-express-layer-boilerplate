import chalk from 'chalk';
import { logInfo, logDebug } from './logger';
import CustomError from '../errorHandle/customError';

class logStep {
  constructor(topic, all, level) {
    this.topic = topic;
    this.all = all;
    this.step = 1;
    this.level = level || 'debug';
  }

  mainStep(msg) {
    if (this.level === 'debug') {
      logDebug(`[${chalk.magenta(this.topic)}] Step ${chalk.yellow(`${this.step}/${this.all}`)} : ${msg}`);
    } else if (this.level) {
      logInfo(`[${chalk.magenta(this.topic)}] Step ${chalk.yellow(`${this.step}/${this.all}`)} : ${msg}`);
    } else {
      throw new CustomError({
        customCode: 3000,
        data: 'Level of log not found'
      });
    }
    this.step = this.step + 1;
  }

  subStep(msg) {
    if (this.level === 'debug') {
      logDebug(`---> ${msg}`);
    } else if (this.level === 'info') {
      logInfo(`---> ${msg}`);
    } else {
      throw new CustomError({
        customCode: 3000,
        data: 'Level of log not found'
      });
    }
  }
}

export default logStep;