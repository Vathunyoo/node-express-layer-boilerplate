import {
  format, transports, loggers
} from 'winston';
import path from 'path';
import errorStackParser from 'error-stack-parser';
import chalk from 'chalk';
import CustomError from '../errorHandle/customError';
import config from '../../config';

loggers.add('logInfo', {
  level: config.logger.level,
  format: format.combine(
    format.colorize(),
    format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
    format.printf(info => `${info.timestamp} | ${info.message}`),
  ),
  transports: [new transports.Console()]
});

loggers.add('logError', {
  level: config.logger.level,
  format: format.combine(
    format.colorize(),
    format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
    format.label({
      label: process.mainModule.filename.split(path.sep).slice(-2).join(path.sep)
    }),
    format.printf(info => `[${info.level}] ${info.timestamp} | ${info.message} at ${info.label}`),
  ),
  transports: [new transports.Console()]
});

const logErr = loggers.get('logError').error;
const logWarn = loggers.get('logError').warn;
const logInfo = loggers.get('logInfo').info;
const logDebug = loggers.get('logInfo').debug;

const logErrStack = (err) => {
  const arrErr = errorStackParser.parse(err);
  logInfo(`[${chalk.red(err.code)}] ${err.message}`);
  arrErr.forEach((line) => {
    console.log(` line : ${line.lineNumber} | col : ${line.columnNumber} | functionName : ${line.functionName}`);
    console.log(`    path : ${line.fileName.split(path.sep).slice(-4).join(path.sep)}`);
  });
  console.log('========================================================');
};

const logComponent = (topic, arrKey, arrValue) => {
  try {
    if (arrKey.length === arrValue.length) {
      let result = topic;
      arrKey.forEach((key, i) => {
        result = result.concat(`| ${key}: ${chalk.yellow(arrValue[i])}`);
      });
      logInfo(result);
    } else {
      throw new CustomError({
        customCode: 3000,
        data: `logComponent length of arrKey [${arrKey.length}] and arrValue [${arrValue.length}] are not equal`
      });
    }
  } catch (err) {
    throw (err);
  }
};

const logCompoStr = (topic, arrKey, arrValue) => {
  if (arrKey.length === arrValue.length) {
    let result = topic;
    arrKey.forEach((key, i) => {
      result = result.concat(`| ${key}: ${chalk.yellow(arrValue[i])}`);
    });
    return result;
  }
  throw new CustomError({
    customCode: 3000,
    data: `logComponent length of arrKey [${arrKey.length}] and arrValue [${arrValue.length}] are not equal`
  });
};

export {
  logErr, logWarn, logInfo, logDebug, logErrStack, logComponent, logCompoStr
};