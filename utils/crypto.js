import { config } from 'dotenv';
import crypto from 'crypto';

// const config = require('config');
// const crypto = require('crypto');
config();
const cryptoService = {};

cryptoService.decryptMessage = (encryptData) => {
  const algorithm = process.env.CRYPTO_ALGO;
  const password = process.env.CRYPTO_PASSWORD;
  const key = crypto.scryptSync(password, 'salt', 24);
  const iv = Buffer.alloc(16, 0);
  const decipher = crypto.createDecipheriv(algorithm, key, iv);
  const inputDecoding = process.env.CRYPTO_OUTPUT;
  const outputDecoding = process.env.CRYPTO_INPUT;
  let decrypted = decipher.update(encryptData, inputDecoding, outputDecoding);
  //   decipher.setAutoPadding(false);
  decrypted += decipher.final(outputDecoding);
  //   const decryptObj = JSON.parse(decrypted);
  return decrypted;
};

cryptoService.encryptMessage = (message) => {
  const algorithm = process.env.CRYPTO_ALGO;
  const password = process.env.CRYPTO_PASSWORD;
  const key = crypto.scryptSync(password, 'salt', 24);
  const iv = Buffer.alloc(16, 0);
  const cipher = crypto.createCipheriv(algorithm, key, iv);
  const inputEncoding = process.env.CRYPTO_INPUT;
  const outputEncoding = process.env.CRYPTO_OUTPUT;
  let encrypted = cipher.update(message, inputEncoding, outputEncoding);
  encrypted += cipher.final(outputEncoding);
  return encrypted;
};

cryptoService.hashMessage = (msg) => {
  const hash = crypto.createHash('sha256');
  hash.update(msg);
  return hash.digest('hex');
};

export default cryptoService;